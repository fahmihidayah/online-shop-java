package com.widsons.spme.controller;

import com.widsons.spme.domain.dto.ProductDto;
import com.widsons.spme.domain.response.BaseResponse;
import com.widsons.spme.service.ProductService;
import com.widsons.spme.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by fahmi on 11/1/17.
 */
@RestController
@RequestMapping("/api/user/product")
public class ProductApiController {

    @Autowired
    ProductService productService;

    @RequestMapping("/create")
    public BaseResponse create(ProductDto productDto){
//        productService.save(productDto, Utils.getCurrentUserAccountDetail().getId());
//        productService.save(productDto, Utils.getCurrentUserAccountDetail().getId());
        productService.save(productDto, 2l);
        return new BaseResponse();
    }

}
