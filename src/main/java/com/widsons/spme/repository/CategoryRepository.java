package com.widsons.spme.repository;

import com.widsons.spme.domain.Category;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by fahmi on 10/6/17.
 */
@Repository
public interface CategoryRepository extends PagingAndSortingRepository<Category, Long> {
    Category findByName(String name);
}
