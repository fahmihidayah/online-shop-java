package com.widsons.spme.domain.dto;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by fahmi on 11/2/17.
 */
public class CategoryDtoSimple {
    @NotEmpty
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
