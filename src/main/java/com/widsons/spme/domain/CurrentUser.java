package com.widsons.spme.domain;

import com.widsons.spme.utils.Utils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by fahmi on 10/30/17.
 */
public class CurrentUser extends User {

    private UserAccount userAccount;

    public CurrentUser(UserAccount userAccount) {
        super(userAccount.getEmail(), userAccount.getPassword(), Utils.getSimpleGrantedAuthority(userAccount));
        this.userAccount = userAccount;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public Long getId(){
        return userAccount.getId();
    }

    public List<Role> getRoles(){
        return userAccount.getRoles();
    }
}
