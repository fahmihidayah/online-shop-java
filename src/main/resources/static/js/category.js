$(document).ready(function(){
  console.log("test load");
  $(document).on('click', '.add-modal', function(){
    $('.modal-title').text('add');
    $('#addModal').modal('show');
  });

  $('.modal-footer').on('click', '.add', function(){
    $.post('/api/category/create',
    {
      'name' : $('#category_name').val()
    },
    function(data, status){
        if(data.status == 200){
          location.reload();
        }
    });
  });

  $(document).on('click', '.edit-modal', function(){
    $('.modal-title').text('Edit');
    $('#id_edit').val($(this).data('id'));
    $('#category_name_edit').val($(this).data('name'));
    $('#editModal').modal('show');
  });

  $('.modal-footer').on('click', '.edit', function(){
    $.post('/api/category/update/' + $('#id_edit').val(),
    {
        'name' : $('#category_name_edit').val()
    },
    function(data, status){
        if(data.status == 200){
            location.reload();
        }
    }
    );
  })

  $(document).on('click', '.delete-modal', function(){
    $('#id_delete').val($(this).data('id'));
    $('#category_name_delete').val($(this).data('name'));
    $('#deleteModal').modal('show');
  });

  $('.modal-footer').on('click', '.delete', function(){
    $.post('/api/category/delete/' + $('#id_delete').val(),
    function(data, status){
        if(data.status == 200){
            location.reload();
        }
    }
    )
  })

});
