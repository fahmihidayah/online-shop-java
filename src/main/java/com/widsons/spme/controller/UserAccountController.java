package com.widsons.spme.controller;

import com.widsons.spme.domain.dto.UserAccountDto;
import com.widsons.spme.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by fahmi on 10/6/17.
 */
@Controller
public class UserAccountController {

    @Autowired
    UserAccountService userAccountService;

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signUp(Model model){
        model.addAttribute("userForm", new UserAccountDto());
        return "signup";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signUpPost(@Valid @ModelAttribute UserAccountDto userAccountDto, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            model.addAttribute("userForm", userAccountDto);
            return "signup";
        }
        userAccountService.save(userAccountDto);
        return "success";

    }
}
