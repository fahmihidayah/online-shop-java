package com.widsons.spme.repository;

import com.widsons.spme.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by fahmi on 7/22/17.
 */
@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
    Product findByName(String name);

    Page<Product> findByUserAccountId(Pageable pageable, long userAccountId);
}
