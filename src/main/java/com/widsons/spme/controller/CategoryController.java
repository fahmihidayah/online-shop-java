package com.widsons.spme.controller;

import com.widsons.spme.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.stereotype.Controller;

/**
 * Created by fahmi on 7/22/17.
 */
@Controller
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryRepository categoryRepository;

    @RequestMapping(value = {"/list", "/"})
    public String list(Model model){
        model.addAttribute("data", categoryRepository.findAll());
        return "category_list";
    }

}
