package com.widsons.spme.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.stereotype.Controller;

/**
 * Created by fahmi on 7/22/17.
 */
@Controller
public class HomeController {

    // @RequestMapping("/")
    // public String hello(){
    //     return "hello world";
    // }
    @RequestMapping("/")
    public String header(){
      return "header";
    }


}
