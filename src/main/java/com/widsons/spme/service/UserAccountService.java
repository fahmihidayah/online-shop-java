package com.widsons.spme.service;

import com.widsons.spme.domain.Role;
import com.widsons.spme.domain.UserAccount;
import com.widsons.spme.domain.dto.UserAccountDto;
import com.widsons.spme.repository.RoleRepository;
import com.widsons.spme.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fahmi on 10/6/17.
 */
@Transactional
@Service
public class UserAccountService {

    @Autowired
    UserAccountRepository userAccountRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    public void save(UserAccountDto userAccountDto){
        save(userAccountDto, roleRepository.findByName("USER"));
    }

    public void save(UserAccountDto userAccountDto, Role role){
        UserAccount userAccount = new UserAccount();
        fillAttribute(userAccount, userAccountDto);
        save(userAccount, role);
    }

    public UserAccount findById(long id){
        return userAccountRepository.findOne(id);
    }

    public UserAccountDto findDtoById(long id){
        UserAccountDto userAccountDto = new UserAccountDto();
        UserAccount userAccount = userAccountRepository.findOne(id);
        userAccountDto.setEmail(userAccount.getEmail());
        userAccountDto.setUsername(userAccount.getUsername());
        return userAccountDto;
    }

    public void update(long userId, UserAccountDto userAccountDto, long roleId){
        update(userId, userAccountDto, roleRepository.findOne(roleId));
    }

    public void update(long userId, UserAccountDto userAccountDto, Role role){
        UserAccount userAccount = findById(userId);
        fillAttribute(userAccount, userAccountDto);
        save(userAccount, role);
    }

    public Page<UserAccount> findAll(Pageable pageable){
        return userAccountRepository.findAll(pageable);
    }

    public void delete(long id){
        userAccountRepository.delete(id);
    }

    private void save(UserAccount userAccount, Role role){
        userAccountRepository.save(userAccount);
        userAccount.getRoles().add(role);
        role.getUserAccounts().add(userAccount);
        userAccountRepository.save(userAccount);
    }

    private void fillAttribute(UserAccount userAccount, UserAccountDto userAccountDto){
        userAccount.setEmail(userAccountDto.getEmail());
        userAccount.setPassword(bCryptPasswordEncoder.encode(userAccountDto.getPassword()));
        userAccount.setUsername(userAccountDto.getUsername());
    }
}
