package com.widsons.spme.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by fahmi on 10/22/17.
 */
@Controller
public class TemplateController {

    @RequestMapping("/template1")
    public String template1(){
        return "example_template";
    }


    @RequestMapping("/list/product")
    public String getListProduct(){
        return "product_form";
    }
}
