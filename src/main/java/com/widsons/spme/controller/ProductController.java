package com.widsons.spme.controller;

import com.widsons.spme.domain.UserAccount;
import com.widsons.spme.domain.dto.ProductDto;
import com.widsons.spme.repository.CategoryRepository;
import com.widsons.spme.service.ProductService;
import com.widsons.spme.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by fahmi on 10/27/17.
 */
@Controller
@RequestMapping("/user/product")
public class ProductController {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ProductService productService;

    @RequestMapping(value = {"/list", "/"})
    public String list(Model model){
        model.addAttribute("categories", categoryRepository.findAll());
        model.addAttribute("data", productService.findAll());
        return "list_product_new";
    }

//    @RequestMapping("/create")
//    public String create(Model model){
//        model.addAttribute("data", new ProductDto());
//        model.addAttribute("categories", categoryRepository.findAll());
//        return "product_form";
//    }
//
//    @RequestMapping(value = "/create", method = RequestMethod.POST)
//    public String save(@Valid @ModelAttribute ProductDto productDto, BindingResult bindingResult){
//        if(bindingResult.hasErrors()){
//            return "product_form";
//        }
//        UserAccount userAccount = Utils.getCurrentUserAccountDetail();
//        productService.save(productDto, userAccount.getId());
//        return "redirect:list";
//    }
}
