package com.widsons.spme.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fahmi on 10/3/17.
 */
@Table(name = "role")
@Entity
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private String name;


    @ManyToMany(mappedBy = "roles", cascade = CascadeType.ALL)
    private List<UserAccount> userAccounts = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserAccount> getUserAccounts() {
        return userAccounts;
    }

    public void setUserAccounts(List<UserAccount> userAccounts) {
        this.userAccounts = userAccounts;
    }
}
