package com.widsons.spme.domain.dto;

/**
 * Created by fahmi on 10/31/17.
 */
public class CategoryDto {
    private String id;
    public CategoryDto() {
    }

    public CategoryDto(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
