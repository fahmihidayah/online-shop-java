package com.widsons.spme.initializer;

import com.widsons.spme.domain.Category;
import com.widsons.spme.domain.Product;
import com.widsons.spme.domain.dto.ProductDto;
import com.widsons.spme.repository.CategoryRepository;
import com.widsons.spme.repository.UserAccountRepository;
import com.widsons.spme.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

/**
 * Created by fahmi on 10/6/17.
 */
@Transactional
@Component
public class ProductInitializer {
    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ProductService productService;

    @Autowired
    UserAccountRepository userAccountRepository;


    public void execute(){
        initialCategories();
//        intialProduct();
//        select();
    }

    private void initialCategories(){
        categoryRepository.save(new Category("Kemeja"));
        categoryRepository.save(new Category("Celana Panjang"));
        categoryRepository.save(new Category("Kaos"));
    }


    private void intialProduct(){
        ProductDto productDto = new ProductDto();
        productDto.setName("Kaos Polos ");
        productDto.setDescription("ini cuman kaos polos");
        productDto.setPrice(4000.0);
        productDto.setCount(200);
//        productDto.setCategoriesId(Arrays.asList(1l,3l));
        productService.save(productDto, 2l);
    }

    private void select(){
        Iterable<Product> products = productService.findAll(new PageRequest(5, 1), 2l);
        for(Product p : products){
//            for(int i =0 ;i < 100;i++)
                System.out.println("data prodcut name is  " + p.getName());
        }


    }

}
