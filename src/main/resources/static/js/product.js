$(document).ready(function () {
    console.log("ready");
});

$(document).on('click', '.add-modal', function(){
    console.log( $('#category option:selected').val());
    $('.modal-title').text('Add');
    $('#addModal').modal('show');
});


$(document).on('click', '.add', function(){
    console.log( $('#category option:selected').val());
    $.post('/api/user/product/create',
    {
            'name' : $('#name').val(),
            'description' : $('#description').val(),
            'count' : $('#count').val(),
            'price' : $('#price').val(),
            'categoryId' :  $('#category option:selected').val()
        },
    function(data, status){
        if(data.status == 200){
          location.reload();
        }
    });
  });

//$('.modal-footer').on('click', '.add', function(){
//    console.log( $('#category_id').val());
//    $.post('/api/user/product/create',
//    {
//        'name' : $('#name').val(),
//        'description' : $('#description').val(),
//        'count' : $('#count').val(),
//        'price' : $('#price').val(),
//        'categoriesId' : [ $('#category_id').val()]
//    },
//    function(data, status){
//    console.log(data);
//        if(data.status == 200){
//            location.reload();
//        }
//    }
//    );
//})
