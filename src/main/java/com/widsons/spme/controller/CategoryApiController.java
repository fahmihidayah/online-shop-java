package com.widsons.spme.controller;

import com.widsons.spme.domain.Category;
import com.widsons.spme.domain.dto.CategoryDto;
import com.widsons.spme.domain.dto.CategoryDtoSimple;
import com.widsons.spme.domain.response.BaseResponse;
import com.widsons.spme.repository.CategoryRepository;
import com.widsons.spme.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by fahmi on 11/1/17.
 */
@RestController
@RequestMapping("/api/category")
public class CategoryApiController {

    @Autowired
    CategoryService categoryService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse create(CategoryDtoSimple categoryDtoSimple){
        categoryService.save(categoryDtoSimple);
        return new BaseResponse();
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    public BaseResponse update(CategoryDtoSimple categoryDtoSimple, @PathVariable("id") Long id){
        categoryService.update(categoryDtoSimple, id);
        return new BaseResponse();
    }

    @RequestMapping(value ="/delete/{id}", method = RequestMethod.POST)
    public BaseResponse delete(@PathVariable("id") Long id){
        categoryService.delete(id);
        return new BaseResponse();
    }

}
