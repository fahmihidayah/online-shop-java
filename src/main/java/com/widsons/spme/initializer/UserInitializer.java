package com.widsons.spme.initializer;

import com.widsons.spme.domain.Role;
import com.widsons.spme.domain.dto.UserAccountDto;
import com.widsons.spme.repository.RoleRepository;
import com.widsons.spme.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by fahmi on 10/6/17.
 */
@Component
@Transactional
public class UserInitializer {

    @Autowired
    UserAccountService userAccountService;

    @Autowired
    RoleRepository roleRepository;

    public void execute(){
        initialRole();
        initialUser();
    }

    public void initialRole(){
        Role role = new Role();
        role.setName("ADMIN");
        roleRepository.save(role);
        Role role1 = new Role();
        role1.setName("USER");
        roleRepository.save(role1);
    }


    public void initialUser(){
        UserAccountDto userAccountDto = new UserAccountDto();
        userAccountDto.setEmail("admin@gmil.com");
        userAccountDto.setUsername("admin");
        userAccountDto.setPassword("fahmi");
        userAccountDto.setPasswordConfirmation("fahmi");
        Role role = roleRepository.findByName("ADMIN");
        userAccountService.save(userAccountDto, role);

        UserAccountDto userAccountDto1 = new UserAccountDto();
        userAccountDto1.setEmail("fahmi.hidayah.cs@gmail.com");
        userAccountDto1.setUsername("fahmiok");
        userAccountDto1.setPassword("123456");
        userAccountDto1.setPasswordConfirmation("123456");
        Role role1 = roleRepository.findByName("USER");
        userAccountService.save(userAccountDto1, role1);


    }


}
