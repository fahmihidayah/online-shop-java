package com.widsons.spme.service;

import com.widsons.spme.domain.Category;
import com.widsons.spme.domain.Product;
import com.widsons.spme.domain.UserAccount;
import com.widsons.spme.domain.dto.CategoryDto;
import com.widsons.spme.domain.dto.ProductDto;
import com.widsons.spme.repository.CategoryRepository;
import com.widsons.spme.repository.ProductRepository;
import com.widsons.spme.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by fahmi on 10/6/17.
 */
@Transactional
@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    UserAccountRepository userAccountRepository;

    public void save(ProductDto productDto, Long userId){
        Product product = new Product();
        fillAttribute(product, productDto);
        productRepository.save(product);
        saveCategories(product, productDto.getCategoryId());
        saveUser(product, userId);
    }

    public void update(long productId, ProductDto productDto, long userId){
        Product product = productRepository.findOne(productId);
        fillAttribute(product, productDto);
        productRepository.save(product);
        saveCategories(product, productDto.getCategoryId());
        saveUser(product, userId);
    }

    private void fillAttribute(Product product, ProductDto productDto){
        product.setName(productDto.getName());
        product.setDescription(productDto.getDescription());
        product.setCount(productDto.getCount());
        product.setPrice(productDto.getPrice());
    }

    public void saveCategories(Product product, Long categoriesDtos){
        product.getCategories().clear();
        Category category = categoryRepository.findOne(categoriesDtos);
        product.getCategories().add(category);
        productRepository.save(product);
    }

    public void saveUser(Product product, Long userId){
        UserAccount userAccount = userAccountRepository.findOne(userId);
        product.setUserAccount(userAccount);
        productRepository.save(product);
    }

    public void delete(long id){
        productRepository.delete(id);
    }

    public Page<Product> findAll(Pageable pageable, long userId){
        return productRepository.findByUserAccountId(pageable, userId);
    }

    public Iterable<Product> findAll(){
        return productRepository.findAll();
    }
}
