package com.widsons.spme.utils;

import com.widsons.spme.domain.CurrentUser;
import com.widsons.spme.domain.Role;
import com.widsons.spme.domain.UserAccount;
import org.omg.CORBA.Current;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by fahmi on 10/30/17.
 */
public class Utils {
    public static Set<SimpleGrantedAuthority> getSimpleGrantedAuthority(UserAccount userAccount){
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        for(Role role : userAccount.getRoles()){
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return authorities;
    }

    public static UserAccount getCurrentUserAccountDetail(){
        return ((CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserAccount();
    }
}
