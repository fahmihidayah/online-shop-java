package com.widsons.spme.repository;

import com.widsons.spme.domain.UserAccount;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by fahmi on 10/6/17.
 */
@Repository
public interface UserAccountRepository extends PagingAndSortingRepository<UserAccount, Long> {

    UserAccount findOneByEmail(String email);

    UserAccount findOneByUsername(String username);
}
