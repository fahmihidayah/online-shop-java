package com.widsons.spme.service;

import com.widsons.spme.domain.Category;
import com.widsons.spme.domain.dto.CategoryDtoSimple;
import com.widsons.spme.repository.CategoryRepository;
import org.hibernate.mapping.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by fahmi on 11/2/17.
 */
@Service
@Transactional
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    public void save(CategoryDtoSimple categoryDtoSimple){
        if(isAvailable(categoryDtoSimple.getName()))
            throw new IllegalStateException("Category " + categoryDtoSimple.getName() + " is available");
        categoryRepository.save(createCategoryObject(categoryDtoSimple, null));
    }

    public void update(CategoryDtoSimple categoryDtoSimple, long id){
        if(!isAvailable(id))
            throw new IllegalStateException("Category "+ categoryDtoSimple.getName() + " not available");
        categoryRepository.save(createCategoryObject(categoryDtoSimple, id));
    }

    public void delete(long id){
        categoryRepository.delete(id);
    }

    public Category findById(long id){
        return categoryRepository.findOne(id);
    }

    public Iterable<Category> findAll(){
        return categoryRepository.findAll();
    }

    private Category createCategoryObject(CategoryDtoSimple categoryDtoSimple, Long id){
        Category category = new Category(categoryDtoSimple.getName());
        if(id != null)
            category.setId(id);
        return category;
    }

    public Category findByName(String name){
        return categoryRepository.findByName(name);
    }

    public boolean isAvailable(String name){
        return findByName(name) != null;
    }

    public boolean isAvailable(long id){
        return findById(id) != null;
    }
}
