package com.widsons.spme.domain.response;

/**
 * Created by fahmi on 11/2/17.
 */
public class BaseResponse {
    private Integer status = 200;
    private String message = "Success";

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
